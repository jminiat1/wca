<?php

namespace Tests\Feature;

use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Foundation\Testing\WithFaker;
use Tests\TestCase;

class UserTest extends TestCase
{
    /** @test */
    public function it_should_add_points_for_user()
    {
        $response = $this->json('POST', '/users/1', [
                'value' => 10
            ]);

        $response->assertRedirect();

        $this->assertDatabaseHas('points', [
            'value' => 10,
        ]);
    }
}
