<!doctype html>
<html>
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <script src="https://cdn.tailwindcss.com"></script>
</head>
<body class="m-3">

<a class="text-indigo-600 hover:text-indigo-900" href="/users">&lt; All Users</a>

@if ($errors->any())
    <div class="alert alert-danger">
        <ul>
            @foreach ($errors->all() as $error)
                <li class="mt-2 text-sm text-red-600">{{ $error }}</li>
            @endforeach
        </ul>
    </div>
@endif
<h1 class="text-3xl font-bold mb-5">
    {{ $user->name }}
</h1>
<div class="mb-5">
Current Points: {{ $user->total_points }}
</div>
<form action="{{ route('users.update', $user->id) }}" method="POST">
    @csrf
    <input class="border border-black mt-1 focus:border-indigo-500 focus:ring-indigo-500 sm:text-sm p-1" type="text" placeholder="Enter Points" name="value"/>

    <button
        class="inline-flex items-center px-2.5 py-1.5 border border-transparent text-xs font-medium rounded shadow-sm text-white bg-indigo-600 hover:bg-indigo-700 focus:outline-none focus:ring-2 focus:ring-offset-2 focus:ring-indigo-500"
        type="submit">Add Points
    </button>
</form>

</body>
</html>
