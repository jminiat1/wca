<?php

namespace App\Http\Controllers;

use App\Models\User;
use Illuminate\Http\Request;

class UserController extends Controller
{

    public function index()
    {
        $users = User::all();

        return view('users.index')->with('users', $users);
    }

    public function show($id)
    {
        $user = User::findOrFail($id);

        return view('users.show')
            ->with('user', $user);
    }

    public function update(Request $request, $id)
    {
        $user = User::findOrFail($id);

        $validated = $request->validate([
            'value' => 'required|integer|between:1,100',
        ]);


        $user->points()->create([
            'value' => $validated['value'],
        ]);

        return redirect()->route('users.index');
    }
}
